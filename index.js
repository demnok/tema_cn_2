let http = require('http')
let server = http.createServer(requestHandler)
let port = 5000

let proxyModel = []

server.listen(port, (err) => {
  if(err) { return console.log(err, 'Oooops') }

  console.log(`Server is on ${port}`)
})

function requestHandler(request, response) {
  let body = []

  request.on('data', chunk => body.push(chunk)).on('end', () => {
    let methodResponseBody
    let encodedBody = Buffer.concat(body).toString()
    body = encodedBody ? JSON.parse(encodedBody) : undefined

    let parsedUrl = parseUrl(request.url)

    if (parsedUrl.paramsLength === 1) {
      methodResponseBody = getCollectionResponseForMethodWithBody(request.method, body)
    } else {
      methodResponseBody = getModelResponseForMethodWithBody(request.method, body, parsedUrl)
    }

    response = formatResponseWithBody(response, methodResponseBody)
    response.end()
  })
}

function getResponse(methodType, params) {
  
}

function getModelResponseForMethodWithBody(methodType, body, parsedUrl) {
  console.log(`ENGAGING MODEL ${methodType}`)
  let id = parseInt(parsedUrl.id)
  let element = proxyModel[id]

  let response = {
    body: '',
    message: '',
    status: 200
  }

  if (methodType === 'GET') {
    response.message = element ? 'GET MODEL SUCCESSFULL' : 'GET MODEL FAIL -- Not Found'
    response.status = element ? response.status : 404
    response.body = element
  }

  if (methodType === 'PUT') {
    response.message = element ? 'PUT MODEL REPLACE SUCCESSFULL' : 'PUT MODEL NON-EXISTENT -> CREATE'
    response.status = body.value ? response.status : 204

    if (response.status === 204) { response.message += 'PUT MODEL SUCCESSFULL -- No Content'}

    if(element) {
      proxyModel[id] = body.value

      response.body = {
        id: id,
        value: body.value
      }
    } else {
      body.value ? proxyModel.push(body.value) : {}

      response.body = {
        id: proxyModel.length - 1,
        value: body.value
      }
    }
  }

  if (methodType === 'PATCH') {
    if (element) {
      response.message = body.value ? 'PATCH MODEL SUCCESSFULL -- Created' : 'PATCH MODEL FAIL -- No Content'
      response.status =  body.value ? 200 : 204

      if (body.value) {
        let parsedElement
        let keys = Object.keys(body.value)

        try {
          parsedElement = JSON.parse(element)
        } catch (err) {
          parsedElement = element
        }

        if(typeof(parsedElement) !== 'object') {
          response.status = 404
          response.message = 'PATCH MODEL FAIL -- Invalid'
        }

        keys.forEach(key => parsedElement[key] = body.value[key])

        proxyModel[id] = parsedElement
      }
    } else {
      response.message = element ? 'PATCH MODEL SUCCESSFULL' : 'PATCH MODEL FAIL -- Not Found'
      response.status = 404
    }
  }

  if (methodType === 'POST') {
    response.message = `${methodType} NOT SUPPORTED ON MODEL -- Not Allowed`
    response.status = 405
  }

  if (methodType === 'DELETE') {
    response.message = element ? 'DELETE SUCCESSFULL' : 'DELETE FAIL -- Not Found'
    response.status = element ? response.status : 404
    if (element) { proxyModel[id] = undefined }
  }

  console.log(`MODEL ${methodType} ${response.status}`)
  return response
}

function getCollectionResponseForMethodWithBody(methodType, body) {
  console.log(`ENGAGING COLLECTION ${methodType}`)

  let response = {
    body: '',
    message: '',
    status: 200
  }

  if (methodType === 'GET') {
    response.message = !proxyModel.length ? 'GET COLLECTION - EMPTY' : 'GET COLLECTION SUCCESSFULL'
    response.body = proxyModel
  }

  if (methodType === 'PUT') {
    response.message = body.value ? 'PUT COLLECTION SUCCESSFULL' : 'PUT COLLECTION FAIL -- No Content'
    response.status = body.value ? 200 : 204
    proxyModel = body.value ? body.value : proxyModel
  }

  if (methodType === 'PATCH') {
    response.message = 'PATCH NOT SUPPORTED ON COLLECTIONS -- Not Allowed'
    response.status = 405
  }

  if (methodType === 'POST') {
    response.message = body.value ? 'POST COLLECTION SUCCESSFULL -- Created' : 'POST COLLECTION FAIL -- No Content'
    response.status = body.value ? 201 : 204
    body.value ? proxyModel.push(body.value) : {}

    response.body = {
      id: proxyModel.length - 1,
      value: body.value
    }
  }

  if (methodType === 'DELETE') {
    response.message = 'DELETE SUCCESSFULL'
    proxyModel = []
  }

  console.log(`COLLECTION ${methodType} ${response.status}`)
  return response
}

function parseUrl(url) {
  let parsedUrl = url.split('/')
  console.log('PARSING URL', parsedUrl)

  return {
    model: parsedUrl[1],
    id: parsedUrl[2],
    attribute: parsedUrl[3],
    paramsLength: parsedUrl.length - 1
  }
}

function formatResponseWithBody(response, body) {
  let err = body.err
  delete body.err

  response.writeHead(body.status, {'Content-Type': 'application/json'})
  response.write(JSON.stringify({
    message: body.message,
    body: body,
    err: err
  }))

  return response
}
